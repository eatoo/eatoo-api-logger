import { Controller } from '@nestjs/common';
import { AppService } from './app.service';
import { RedisService } from './slaves/redis/redis.service';
import { FileRequestService } from './slaves/file-request/file-request.service';
import { EventPattern } from '@nestjs/microservices';
import { Payload, PayloadRedis } from '_interfaces/payload.interface';
import { EVENTS } from './constants';
import { SlaveTypes } from '_interfaces/slave.interface';

@Controller()
export class AppController {
  constructor(
    private readonly appService: AppService,
    private readonly redisService: RedisService,
    private readonly fileRequestService: FileRequestService,
  ) {
    appService.register({
      name: EVENTS.REDIS,
      instance: redisService,
      type: SlaveTypes.REDIS,
    });
    appService.register({
      name: EVENTS.FILE_REQUEST,
      instance: fileRequestService,
      type: SlaveTypes.FILE_REQUEST,
    });
  }
  
  @EventPattern(EVENTS.ALL)
  eventAll(payload: Payload): boolean {
    return this.appService.processAll(payload);
  }
  
  @EventPattern(EVENTS.REDIS)
  eventRedis(payload: PayloadRedis): boolean {
    return this.appService.processRedis(payload);
  }
  
  @EventPattern(EVENTS.FILE_REQUEST)
  eventFileRequest(payload: Payload): boolean {
    return this.appService.processFileRequest(payload);
  }
  
  @EventPattern(EVENTS.ERROR)
  eventError(payload: Payload): boolean {
    return this.appService.processError(payload);
  }
}
