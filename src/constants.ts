export const EVENTS = {
  ALL: 'ALL',
  REDIS: 'REDIS',
  FILE_REQUEST: 'FILE_REQUEST',
  ERROR: 'ERROR',
};
