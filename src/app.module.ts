import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { RedisModule } from './slaves/redis/redis.module';
import { FileRequestModule } from './slaves/file-request/file-request.module';

@Module({
  imports: [RedisModule, FileRequestModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
