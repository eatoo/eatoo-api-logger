import {NestFactory} from '@nestjs/core';
import {AppModule} from './app.module';
import {Logger} from "@nestjs/common";
import {Transport} from "@nestjs/microservices";


async function bootstrap() {
  const app = await NestFactory.createMicroservice(AppModule, {
    transport: Transport.TCP,
    options: {
      host: 'localhost',
      port: parseInt(process.env.PORT, 10) || 6373,
    },
  });
  await app.listen(() => Logger.log('Logger Microservice is listening.'));
}
bootstrap();
