export interface Payload {
  data: object;
  type?: number;
  options?: object;
}

export interface PayloadRedis extends Payload {
  /**
   * A string indicating which redis date value to increment
   * Possible values : "d" -> day, "m" -> month, "y" -> year
   * - "d"
   * - "m"
   * - "dm"
   * - "ydm"
   * etc.
   */
  dateIncrements?: string;
}
