import { Payload } from './payload.interface';

export enum SlaveTypes {
  ALL = 0,
  REDIS = 1,
  FILE_REQUEST = 2,
  ERROR = 4,
}

export interface SlaveInstance {
  process(payload: Payload): boolean;
}

export interface Slave {
  name: string;
  instance: SlaveInstance;
  type: SlaveTypes;
}
