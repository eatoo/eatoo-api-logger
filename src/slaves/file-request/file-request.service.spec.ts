import { Test, TestingModule } from '@nestjs/testing';
import { FileRequestService } from './file-request.service';

describe('FileRequestService', () => {
  let service: FileRequestService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [FileRequestService],
    }).compile();

    service = module.get<FileRequestService>(FileRequestService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
