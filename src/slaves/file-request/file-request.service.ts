import { Injectable, Logger } from '@nestjs/common';
import { Payload } from '_interfaces/payload.interface';

@Injectable()
export class FileRequestService {
  process(payload: Payload): boolean {
    Logger.debug('Coucou je suis ici', 'SLAVE/FILE_REQUEST');
    return true;
  }
}
