import { Injectable, Logger } from '@nestjs/common';
import { PayloadRedis } from '_interfaces/payload.interface';

enum DateIncrement {
  DAY,
  MONTH,
  YEAR,
}

const DateIncrementDef: object = {
  d: DateIncrement.DAY,
  m: DateIncrement.MONTH,
  y: DateIncrement.YEAR,
};

@Injectable()
export class RedisService {
  private _parseDateIncrements(list?: string): DateIncrement[] {
    const increments: DateIncrement[] = [];
    if (!list) {
      return [];
    }
    list.split('').forEach(inc => {
      if (DateIncrementDef.hasOwnProperty(inc)) {
        increments.push(DateIncrementDef[inc]);
      }
    });
    return increments;
  }
  
  process(payload: PayloadRedis): boolean {
    const increments = this._parseDateIncrements(payload.dateIncrements);
    Logger.debug(
      'Coucou je suis un redis' + JSON.stringify(payload.data),
      'SLAVE/REDIS',
    );
    if (increments.includes(DateIncrement.DAY)) {
      Logger.warn('DAY', 'SLAVE/REDIS');
    }
    if (increments.includes(DateIncrement.MONTH)) {
      Logger.warn('MONTH', 'SLAVE/REDIS');
    }
    if (increments.includes(DateIncrement.YEAR)) {
      Logger.warn('YEAR', 'SLAVE/REDIS');
    }
    return true;
  }
}
