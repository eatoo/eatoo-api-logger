import { Injectable } from '@nestjs/common';
import { Slave, SlaveTypes } from '_interfaces/slave.interface';
import { Payload, PayloadRedis } from '_interfaces/payload.interface';

@Injectable()
export class AppService {
  private slaves: Slave[] = [];
  
  constructor() {
  }
  
  register(slave: Slave): void {
    this.slaves.push(slave);
  }
  
  processAll(payload: Payload): boolean {
    this.slaves.forEach(slave => slave.instance.process(payload));
    return true;
  }
  
  processRedis(payload: PayloadRedis): boolean {
    return this._executeByType(SlaveTypes.REDIS, slave => {
      return slave.instance.process(payload);
    });
  }
  
  processFileRequest(payload: Payload): boolean {
    return this._executeByType(SlaveTypes.FILE_REQUEST, slave => {
      return slave.instance.process(payload);
    });
  }
  
  processError(payload: Payload): boolean {
    return this._executeByType(SlaveTypes.ERROR, slave => {
      return slave.instance.process(payload);
    });
  }
  
  private _executeByType(type: SlaveTypes, fn: (slave: Slave) => boolean): boolean {
    return this._filterByType(type).some(slave => fn(slave));
  }
  
  private _filterByType(type: SlaveTypes): Slave[] {
    return this.slaves.filter(slave => slave.type & type);
  }
  
  private _findByType(type: SlaveTypes): Slave {
    return this.slaves.find(slave => slave.type & type);
  }
  
  private _findByName(name: string): Slave {
    return this.slaves.find(slave => slave.name === name);
  }
}
